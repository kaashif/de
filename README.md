Differential Equations Coursework
=================================
To compile, just use pdflatex or something.

License
-------
This work is licensed under the Creative Commons
Attribution-NonCommercial license. A copy of this license can be found
in the LICENSE file.
