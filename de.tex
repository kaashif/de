\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{parskip,pgfplots,pgfplotstable,tikz}
\usepackage[margin=1in]{geometry}
\usepackage{titlesec,amsmath,listings,filecontents}
\newcommand{\sectionbreak}{\clearpage}

\title{Modelling the landing of an aircraft}
\author{Kaashif Hymabaccus}
\date{}

\pgfplotstableset{
  alias/v (data)/.initial=vd,
  alias/v (model)/.initial=vm,
  alias/$d^2$/.initial=d,
}

\pgfplotsset{
  compat=1.8
}

\newcommand{\plotandtable}[1]{
\begin{minipage}[c]{0.65\textwidth}
\begin{tikzpicture}
\begin{axis}[
    grid=both,
    width = 0.8\textwidth,
    height = \textwidth,
    axis lines = left,
    xlabel = $t$,
    ylabel = $v$
]
\addplot+[
    color=blue,
    mark=x,
    only marks,
    scatter/@pre marker code/.style={/tikz/mark size=\pgfplotspointmeta/50},
    scatter/@post marker code/.style={}
] table [y=vd] {#1};
\addlegendentry{$v$ (data)}
\addplot+[
    color=red,
    mark=x,
    only marks,
    scatter/@pre marker code/.style={/tikz/mark size=\pgfplotspointmeta/50},
    scatter/@post marker code/.style={}
] table [y=vm] {#1};
\addlegendentry{$v$ (model)}
\end{axis}
\end{tikzpicture}
\end{minipage}
\begin{minipage}[c]{0.1\textwidth}
\pgfplotstabletypeset[col sep=space,
  columns={t, v (data), v (model), $d^2$},
  precision=4,
  dec sep align,
  fixed,
  columns/$d^2$/.style={zerofill},
  columns/v (model)/.style={zerofill},
  ]{#1}
\end{minipage}}

\begin{document}
\maketitle
\tableofcontents

\section{Initial model}
\subsection{Assumptions}
\begin{minipage}[c]{0.49\textwidth}
\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (0,2) -- (2,2) -- (2,0) -- cycle;
\draw (0.1,1) node[anchor=west]{Aeroplane};
\draw[thick,->] (0,1) -- (-2,1) node[anchor=east]{Resistance};
\draw[thick,->] (1,0) -- (1,-2) node[anchor=north]{Weight};
\draw[thick,->] (1,2) -- (1,4) node[anchor=south]{Normal};
\end{tikzpicture}
$$t=0$$
\end{center}
\end{minipage}
\begin{minipage}[c]{0.49\textwidth}
\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (0,2) -- (2,2) -- (2,0) -- cycle;
\draw (0.1,1) node[anchor=west]{Aeroplane};
\draw[thick,->] (0,1.5) -- (-2,1.5) node[anchor=east]{Resistance};
\draw[thick,->] (0,0.5) -- (-2,0.5) node[anchor=east]{Braking Force};
\draw[thick,->] (1,0) -- (1,-2) node[anchor=north]{Weight};
\draw[thick,->] (1,2) -- (1,4) node[anchor=south]{Normal};
\end{tikzpicture}
$$t=t_b$$
\end{center}
\end{minipage}
\vspace{2em}

The first assumption I will make is that $R \propto v$. This is the
most important assumption, as it will heavily influence the form of
the solutions to the equations. This assumption is justified, as it
is obvious that as the velocity of the plane increases, the resistance
force increases. The simplest way to model this relationship is to say
that they are proportional to each other. This is the most
important assumption, as it will dictate the form of the general
solutions to any differential equation obtained.

The second assumption is that there is a constant braking force applied
at $t=t_b$, where $t_b$ is to be determined. This assumption
is justified, as the plane must deploy flaps, brakes, or some other
mechanism to slow the plane to a halt before it overruns the runway.
This assumption is important as it will heavily influence the values
of the constants in the equation, and may have an impact on the form
of the general solution.

There are several other assumptions we must make to be able to model the
landing of the plane:

\begin{itemize}
\item There is no headwind, tailwind or crosswind. We have no information on the magnitude or direction of any force that the wind would cause, so we must assume there is wither no wind or such a low wind speed that it would cause no significant acceleration in any direction.
\item The mass of the plane is constant. It is possible that the plane could be dumping fuel, but we are given no information on this, so we have no choice but to assume the mass of the plane remains constant.
\item The runway is perfectly level. If this were not the case, the weight of the plane would have a component acting opposite to the plane's direction of motion. We are not told anything about the slope of the runway, therefore we must assume it is level.
\item The runway's surface remains consistent throughout the landing. We must discount the possibility that parts of the runway are worn down, frozen over, or wet, as this would result in the coefficient of friction of the surface changing, which would change the resistance force. In order to model the situation, we must ignore this possibility.
\item The plane does not deploy brakes \emph{after} deploying flaps or employ any braking method that would result in anything but a constant force. In reality, it is likely that the braking force would also depend, in some way, on the velocity of the plane, but it would be almost impossible to model the effects of flaps, landing gear, and other braking methods without more information. This means the braking force must be assumed to be constant.
\end{itemize}

All of these additional assumptions are aspects of the situation that 
would be taken into account
if the information were available, but it is not, meaning they must
be ignored.

Obviously, the first set of assumptions (regarding the braking force being
applied and that $R \propto v$) is more important, as it is likely that,
even with all of the second set of assumptions removed and everything
considered, the effect of wind, fuel dumping, runway surface and so on would
still be negligible.

\begin{minipage}[c]{0.69\textwidth}
\begin{tikzpicture}
\begin{axis}[
    grid=both,
    width = 0.8\textwidth,
    height = \textwidth,
    axis lines = left,
    xlabel = $t$,
    ylabel = $v$
]

\addplot[
    color=blue,
    only marks,
    mark=x,
    scatter/@pre marker code/.style={/tikz/mark size=\pgfplotspointmeta/50},
    scatter/@post marker code/.style={}
] coordinates {(0,96)(1,89)(2,82)(3,77)(4,72)(5,68)(6,64)(7,61)(8,58)(9,55)(10,50)(11,46)(12,41)(13,38)(14,34)(15,31)(16,27)(17,24)(18,21)(19,18)(20,16)(21,13)(22,10)(23,8)(24,5)(25,3)(26,0)};
\addlegendentry{$v$ (data)}

\end{axis}
\end{tikzpicture}
\end{minipage}
\begin{minipage}[c]{0.29\textwidth}
\begin{tabular}{lll}
$t$ & $v$ (data) & $\Delta v$ \\
0 & 96 &  \\
1 & 89 & -7 \\
2 & 82 & -7 \\
3 & 77 & -5 \\
4 & 72 & -5 \\
5 & 68 & -4 \\
6 & 64 & -4 \\
7 & 61 & -3 \\
8 & 58 & -3 \\
9 & 55 & -3 \\
10 & 50 & -5 \\
11 & 46 & -4 \\
12 & 41 & -5 \\
13 & 38 & -3 \\
14 & 34 & -4 \\
15 & 31 & -3 \\
16 & 27 & -4 \\
17 & 24 & -3 \\
18 & 21 & -3 \\
19 & 18 & -3 \\
20 & 16 & -2 \\
21 & 13 & -3 \\
22 & 10 & -3 \\
23 & 8 & -2 \\
24 & 5 & -3 \\
25 & 3 & -2 \\
26 & 0 & -3
\end{tabular}
\end{minipage}
\vspace{2em}

From an examination of $\Delta v$ (representing the change in $v$ from the 
previous value of $v$), we can see
that $\Delta v$ follows a trend of decreasing in magnitude by 1 ms$^{-1}$
every 2 seconds until $t=9$, when $\Delta v$ increases in
magnitude from $t=9$ to $t=10$.

This change in trend supports the idea that the situation must be
modelled by two distinct equations, the first modelling the plane's
motion before the brakes are applied, and the second modelling the
motion after they are applied, with the change from one equation to
the other occurring between $t=9$ and $t=10$.

This examination of $\Delta v$ leads to the decision that $t_b=9$,
meaning the
time the brakes are applied appears to be 9 seconds. This is more useful
than saying the time the brakes are applied is somewhere in between 9 and 10
seconds, as this gives us a boundary condition for both equations we are
sure is correct (assuming the data is valid).

\subsection{Creating and solving the equations}

For $t<9$, we can resolve the forces on the aircraft, taking the direction
of movement to be the positive direction. Using Newton's second law gives
us a differential equation that can be solved by separating the variables:

\begin{align*}
F &= ma \\
-R &= 120000 \frac{dv}{dt} \\
-kv &= 120000 \frac{dv}{dt} \\
-\int k \; dt &= 120000 \int \frac{1}{v} \; dv \\
-kt &= 120000 \ln v + c \\
v &= Ae^{-\frac{kt}{120000}}
\end{align*}

For $t \geq 9$, we can again use the same technique, using Newton's
second law with the forward direction as positive. This gives us a
differential equation that can be solved by separating the variables.

\begin{align*}
F &= ma \\
- R - B &= 120000 \frac{dv}{dt} \\
- kv - B &= 120000 \frac{dv}{dt} \\
\int 1 \; dt &= - 120000 \int \frac{1}{kv + B} \; dv \\
t &= -\frac{120000}{k} \ln(kv+B) + c\\
v &= \frac{A}{k}e^{-\frac{tk}{120000}}-\frac{B}{k}
\end{align*}

\subsection{Determining the values of the parameters}

In order to determine the values of the parameters in the model, it 
is necessary to perform a regression analysis, aimed at minimising the
sum of the squares of the differences between the model's line and the 
data points (the residuals). To do this for the first part of the model, we 
must take the natural logarithm of each side of the equation, giving 
us a linear relationship between $\ln v$ and $t$.

$$\ln v = \ln A - \frac{kt}{120000}$$

\newpage

The values that are thought to follow this relationship are tabulated
below:

\begin{center}
\begin{tabular}{lll}
t & v & ln v \\
0 & 96 & 4.5643 \\
1 & 89 & 4.4886 \\
2 & 82 & 4.4067 \\
3 & 77 & 4.3438 \\
4 & 72 & 4.2767 \\
5 & 68 & 4.2195 \\
6 & 64 & 4.1589 \\
7 & 61 & 4.1109 \\
8 & 58 & 4.0604 \\
9 & 55 & 4.0073
\end{tabular}
\end{center}
\vspace{1em}

The values required to perform the analysis are (taking the variable
representing $\ln v$ to be $l$, for readability):

\begin{align*}
\overline{l} &= 4.2637 \\
\overline{t} &= 4.5 \\
S_{tt} &= \Sigma t^2 - n\overline{t}^2 \\
       &= 285 - 10 \times 4.5^2 \\
       &= 82.5 \\
S_{lt} &= \Sigma lt - n \overline{lt} \\
     &= 186.8166524 - 10 \times 4.5 \times 4.2637 \\
     &= -5.0508
\end{align*}

This means the least squares regression line is:
\begin{align*}
\ln v - \overline{\ln v} &= \frac{S_{lt}}{S_{tt}}(t - \overline{t}) \\
\ln v &=  -0.06122t + 4.539
\end{align*}

This means the values for $A$ and $k$ are:

\begin{align*}
A &= e^{4.539} \\
  &= 93.6178 \\
k &= -120000 \times -0.06122 \\
  &= 7346.6479
\end{align*}

This means the final equation for $t<9$ is:

$$v = 93.6178e^{-0.06122t}$$

For the second part of the model, the solution for the differential
equation is of a slightly different form:

$$v = \frac{A}{k}e^{-\frac{tk}{120000}}-\frac{B}{k}$$

This means it cannot be linearised easily, and a numerical
method must be used to minimise the sum of the squares of the
residuals.

In this case, the chosen numerical method was the Levenberg-Marquadt
algorithm, which is used to minimise the sum of the squared residuals
of a non-linear relationship. Essentially, the algorithm works by finding
a local minimum of the function that calculates the sum of the squared
residuals, $S(\boldsymbol{\beta})$, given a parameter vector $\boldsymbol{\beta}$ 
(of appropriate 
dimension), and a number, $m$, of data points, solving the relationship:

$$
\frac{\partial S(\boldsymbol{\beta})}{\partial \boldsymbol{\beta}} = 0 \text{   where } S(\boldsymbol{\beta}) = \sum_{i=1}^{m} (y_i - f(x_i, \boldsymbol{\beta}))^2
$$

Actually solving this is very complex and requires maths outside the
course, therefore is best left to a computer. A short program was written
in Python to solve the above relationship and output an optimal set of
parameters.

\begin{lstlisting}
import numpy as np
from scipy.optimize import curve_fit

def func(t, a, b, c):
    return a * np.exp(-b * t) + c

tdata = range(9,26)
vdata = [96, 89, 82, 77, 72,
         68, 64, 61, 58, 55,
         50, 46, 41, 38, 34,
         31, 27, 24, 21, 18,
         16, 13, 10,  8,  5,
          3][9:]

popt, _ = curve_fit(func, tdata, vdata)

print("%fe^-%ft + %f" % (popt[0], popt[1], popt[2]))
\end{lstlisting}

The program produced the following equation:

$$v = 147.9436e^{-0.04757t} - 41.8207$$

This means the final two equations for the first model are:

$$
v = 
\begin{cases}
  93.6178e^{-0.06122t} & \mbox{if } t < 9 \\
  147.9436e^{-0.04757t} - 41.8207 & \mbox{if } t \geq 9
\end{cases}
$$

\subsection{Comparison with data}

\begin{filecontents}{first.txt}
t	vd	vm      d
0	96	93.6178	5.6749
1	89	88.0584	0.8866
2	82	82.8292	0.6876
3	77	77.9105	0.8290
4	72	73.2839	1.6483
5	68	68.9320	0.8686
6	64	64.8386	0.7032
7	61	60.9882	0.0001
8	58	57.3665	0.4013
9	55	53.9599	1.0819
10	50	50.1179	0.0139
11	46	45.8467	0.0235
12	41	41.7739	0.5989
13	38	37.8903	0.0120
14	34	34.1872	0.0350
15	31	30.6560	0.1183
16	27	27.2890	0.0835
17	24	24.0783	0.0061
18	21	21.0168	0.0003
19	18	18.0976	0.0095
20	16	15.3139	0.4707
21	13	12.6596	0.1159
22	10	10.1286	0.0165
23	8	7.7152	0.0811
24	5	5.4139	0.1713
25	3	3.2195	0.0482
26	0	1.1271	1.2703
\end{filecontents}

\plotandtable{first.txt}

\vspace{5em}

When $t=0$, the difference in the predicted and actual velocities is
$2.38$, which is very large. When the plane stops completely, and the
data says that $v=0$, the model predicts that $v=1.127$, which is
off by a significant amount. Considering that regression
analysis has been used to minimise differences as much as possible,
these large differences suggest the model is incorrect, and that the
regression analysis has merely given the best possible parameters to
fit the incorrect model with the data.

It is obvious that the first part of the model has been forced to pass
through the mean time and mean $\ln v$, and that the second part of
the model has also been forced to pass through a central point
(although, due to the difficulty rearranging the equation into a
linear form, it is difficult to work out which point it has been
forced through). This results in large errors at the start and end of
each part of the model, which can clearly be seen at $t=$0, 9, and 26,
where $d^2$ is very large. This adds to the evidence that this model
is not correct.

The sum of the squares of the residuals (denoted in the table above as
$d^2$) is $15.9$, which is quite large. This is not immediately
useful, but will be helpful for evaluating this model in comparison to
others.

\subsection{Other strategies for selecting parameter values}
\subsubsection{Substituting known values for $v$ and $t$}

The most obvious way to obtain an equation for a curve with known
points is to substitute points from the data into the equation, and
solve the resulting simultaneous equations for the constants. Doing
this for the first equation and substituting the values of $v$ for
when $t=0$ and $t=9$ gives:0

\begin{align*}
v &= ae^{bt} \\
\text{(when $t=0$)}\;\;96 &= a \\
\text{(when $t=9$)}\;\;55 &= ae^{9b}
\end{align*}

Immediately, this results in values of $a=96$ and
$b=\frac{1}{9}\ln \frac{55}{96}$.

For the second part of the model, the same thing can be done:

\begin{align*}
v &= ae^{bt} + c \\
\text{(when $t=9$)}\;\; 55 &= ae^{9b} + c\\
\text{(when $t=10$)}\;\; 50 &= ae^{10b} + c \\
\text{(when $t=26$)}\;\; 0 &= ae^{26b} + c\\
\end{align*}

Solving these equations gives $a=147.6324$, $b=-0.05972$ and
$c=-31.2504$. This means the two new equations are:

$$
v = 
\begin{cases}
  96e^{-0.06189t} & \mbox{if } t < 9 \\
  147.6234e^{-0.05972t} - 41.8207 & \mbox{if } t \geq 9
\end{cases}
$$

\begin{filecontents}{first_worse.txt}
t	vd	vm	d
0	96	96.0000	0.0000
1	89	90.2386	1.5342
2	82	84.8230	7.9695
3	77	79.7324	7.4662
4	72	74.9474	8.6869
5	68	70.4494	5.9998
6	64	66.2215	4.9350
7	61	62.2472	1.5556
8	58	58.5115	0.2617
9	55	55.0000	0.0000
10	50	50.0000	0.0000
11	46	45.2899	0.5043
12	41	40.8528	0.0217
13	38	36.6729	1.7612
14	34	32.7353	1.5994
15	31	29.0260	3.8966
16	27	25.5317	2.1558
17	24	22.2400	3.0974
18	21	19.1392	3.4627
19	18	16.2180	3.1754
20	16	13.4663	6.4198
21	13	10.8740	4.5199
22	10	8.4320	2.4586
23	8	6.1316	3.4909
24	5	3.9645	1.0722
25	3	1.9231	1.1597
26	0	0.0000	0.0000
\end{filecontents}

\plotandtable{first_worse.txt}

From looking at the plot of the predicted velocities against the data,
it is very obvious that the graph has been forced through the points
when $t=0$, $t=9$, $t=10$ and $t=26$, as the value of $d^2$ is zero at
those times. $\Sigma d^2 = 77.2$, confirming the fact that
substituting points is not a good strategy for selecting the
parameters, as this is a lot greater than the $15.9$ obtained before,
meaning this strategy has resulted in a far worse fit than the
previous strategy.

\subsubsection{Correcting assumptions made about the distribution of errors in $v$}

When linearising the equation by taking the logarithm of both sides of
the equation, we have assumed the errors in $\ln v$ are normally
distributed. This becomes apparent when examining the manipulation of
the equation and the effect on a heretofore not considered error term.

\begin{align*}
v & = ae^{bt} + \epsilon_0 \\
\ln v &= \ln a + \epsilon_1 + bt
\end{align*}

By considering the second equation suitable for linear regression, we
have made the assumption that $\epsilon_1$ is normally distributed
(random, with constant variance and mean). This may not be the
case. For example, if we assume that the errors in $v$, $\epsilon_0$
are normally distributed (which is likely, considering $v$ would have
been measured with an instrument), taking the natural
logarithm of the errors, as has been done, will not result in another
normal distribution of errors. This means that minimising $\Sigma d^2$ 
for the
second equation may not result in minimising $\Sigma d^2$ for the
first equation.

There are two possible solutions. The first is to work out the true
distribution of errors in $\ln v$ and minimise the sum of the squared
residuals multiplied by some weighting function determined by the
distribution.

Unfortunately, the rounding of the values for $v$ means that the real
distribution of the errors in $v$ is impossible to determine, as the
real values may be significantly different to the values given, and
the differences obscured by rounding.

The second, more practical, is to make no assumptions about the errors
and use a numerical method. The Levenburg-Marquadt algorithm was
applied using the same program as before, and the following equation 
was deemed to be the best fit:

$$v = 94.2311e^{-0.06283t}$$

Using this new equation for $t \leq 9$ resulted in the following.

\begin{filecontents}{first_better.txt}
t	vd	vm	d
0	96	94.2311	3.1291
1	89	88.4924	0.2576
2	82	83.1033	1.2172
3	77	78.0423	1.0864
4	72	73.2896	1.6630
5	68	68.8263	0.6827
6	64	64.6348	0.4030
7	61	60.6986	0.0909
8	58	57.0020	0.9959
9	55	53.5306	2.1590
10	50	50.1179	0.0139
11	46	45.8467	0.0235
12	41	41.7739	0.5989
13	38	37.8903	0.0120
14	34	34.1872	0.0350
15	31	30.6560	0.1183
16	27	27.2890	0.0835
17	24	24.0783	0.0061
18	21	21.0168	0.0003
19	18	18.0976	0.0095
20	16	15.3139	0.4707
21	13	12.6596	0.1159
22	10	10.1286	0.0165
23	8	7.7152	0.0811
24	5	5.4139	0.1713
25	3	3.2195	0.0482
26	0	1.1271	1.2703
\end{filecontents}

\plotandtable{first_better.txt}
\vspace{1em}

With the new parameters, $\Sigma d^2 = 14.8$, meaning the model has
been improved from the previous value of $\Sigma d^2 = 15.9$.

Despite the decrease in $\Sigma d^2$, its value still remains quite
large, and the same criticisms of this model can be made, namely that
the graph has been forced through points, resulting in large errors for
some values of $t$ and small errors for the forced values. In
conclusion, despite all possible methods of choosing parameters being
explored, the model still fails to provide a good fit to the data,
meaning it must be incorrect in its assumptions.

\section{Revision of the model}
\subsection{Assumptions}

\begin{minipage}[c]{0.49\textwidth}
\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (0,2) -- (2,2) -- (2,0) -- cycle;
\draw (0.1,1) node[anchor=west]{Aeroplane};
\draw[thick,->] (0,1) -- (-2,1) node[anchor=east]{Resistance};
\draw[thick,->] (1,0) -- (1,-2) node[anchor=north]{Weight};
\draw[thick,->] (1,2) -- (1,4) node[anchor=south]{Normal};
\end{tikzpicture}
$$t=0$$
\end{center}
\end{minipage}
\begin{minipage}[c]{0.49\textwidth}
\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (0,2) -- (2,2) -- (2,0) -- cycle;
\draw (0.1,1) node[anchor=west]{Aeroplane};
\draw[thick,->] (0,1.5) -- (-2,1.5) node[anchor=east]{Resistance};
\draw[thick,->] (0,0.5) -- (-2,0.5) node[anchor=east]{Braking Force};
\draw[thick,->] (1,0) -- (1,-2) node[anchor=north]{Weight};
\draw[thick,->] (1,2) -- (1,4) node[anchor=south]{Normal};
\end{tikzpicture}
$$t=t_b$$
\end{center}
\end{minipage}
\vspace{2em}

The same assumptions about wind, mass, friction and so on from the
first model still apply. The assumption that must be modified is that
resistance is proportional to velocity.

It is reasonable to suppose that air resistance depends on the
cross-sectional area of the moving object, the density of the air it
is moving through, and the velocity of the object. It is not known how
the air resistance force depends on these factors, so dimensional
analysis must be used. Taking $k$ to be a dimensionless constant:

\begin{align*}
R &= k\rho^\alpha v^\beta A^\gamma \\
MLT^{-2} &= (ML^{-3})^\alpha (LT^{-1})^\beta (L^2)^\gamma
\end{align*}
$$\alpha = 1,\;\beta = 2,\;\gamma = 1$$

This means that the resistance force must be proportional to $v^2$,
and not $v$, as was originally thought.

\subsection{Creating and solving the equations}

As before, it is evident that the brakes are applied at $t=9$, and
the same argument can be made.

Using Newton's second law, taking the direction of movement to be the
positive direction:

\begin{align*}
F &= ma \\
- R &= 120000 \; \frac{dv}{dt}
\end{align*}

Assuming $R \propto v^2$ means $R = kv^2$, where $k$ is a parameter
that depends factors such as the coefficient of friction of the
runway's surface and the drag coefficient of the air the plane is
travelling through. This results in:

$$- kv^2 = 120000 \; \frac{dv}{dt}$$

This differential equation can be solved as follows, by separation of
the variables:

\begin{align*}
-\int k\;dt &= 120000 \, \int \frac{1}{v^2}\;dv \\
- kt &= -\frac{120000}{v} + c\\
v &= \frac{120000}{kt + c}
\end{align*}

For the second part of the graph, if we again take the direction of
travel to be positive and use Newton's Second Law, we obtain another
differential equation that can again be solved by separation:

\begin{align*}
F &= ma \\
- R - B &= 120000 \, \frac{dv}{dt} \\
- kv^2 - B &= 120000 \, \frac{dv}{dt} \\
- \int 1\;dt &= 120000 \int \frac{1}{kv^2+B} \; dv \\
- t + c &= \frac{120000}{k} \int \frac{1}{v^2+\frac{B}{k}} \; dv \\
- t + c &= \frac{120000}{\sqrt{Bk}} \arctan{\frac{v\sqrt{k}}{\sqrt{B}}}\\
v &= \sqrt{\frac{B}{k}} \tan{\left( \frac{\sqrt{Bk}}{120000}t - c \right)}
\end{align*}

\subsection{Determining the values of the parameters}

As before, regression analysis must be used to obtain the ideal
parameters.  This can be done by rearranging the first part of the
model into the following form:

\begin{align*}
v &= \frac{120000}{kt + c} \\
\frac{120000}{v} &= kt + c
\end{align*}

Taking the variable representing $\frac{120000}{v}$ to be $y$, the
following results were obtained:

\begin{minipage}[c]{0.49\textwidth}
\centering
\begin{tabular}{lll}
t & v (data) & y \\
0 & 96 & 1250.0000 \\
1 & 89 & 1348.3146 \\
2 & 82 & 1463.4146 \\
3 & 77 & 1558.4416 \\
4 & 72 & 1666.6667 \\
5 & 68 & 1764.7059 \\
6 & 64 & 1875.0000 \\
7 & 61 & 1967.2131 \\
8 & 58 & 2068.9655 \\
9 & 55 & 2181.8182
\end{tabular}
\end{minipage}
\begin{minipage}[c]{0.49\textwidth}
\begin{align*}
S_{yt} &= \Sigma yt - n\overline{yt} \\
       &= 8498.8 \\
S_{tt} &= \Sigma t^2 - n\overline{t}^2 \\
&= 82.5 \\
\;&\\
y-\overline{y} &= \frac{S_{yt}}{S_{tt}}(t - \overline{t}) \\
y &= 103.016t + 1250.882
\end{align*}
\end{minipage}

\vspace{1em}

This means the final values are $k=103.016$ and $c=1250.882$
and the final equation for the first part of the model is:

$$v = \frac{120000}{103.016t + 1250.882}$$

The second equation was found to be in the form (with all unknown
constants reduced to single letters):

$$v = a \tan (bt - c)$$

This cannot be rearranged to form a linear relationship, meaning a
numerical approach must be taken.

The following program was run to obtain values for $a$, $b$ and $c$.

\begin{lstlisting}
import numpy as np
from scipy.optimize import curve_fit

def func(t, a, b, c):
    return a*np.tan(b*t - c)

tdata = range(9,26)
vdata = [96, 89, 82, 77, 72,
         68, 64, 61, 58, 55,
         50, 46, 41, 38, 34,
         31, 27, 24, 21, 18,
         16, 13, 10,  8,  5,
          3][9:]

popt, _ = curve_fit(func, tdata, vdata)

print("a = %f, b = %f, c = %f" % (popt[0], popt[1], popt[2]))
\end{lstlisting}

The program output the following equation.

$$v = -53.542844 \tan (0.046874t - 1.221335)$$

This means the final two equations are:

$$
v = 
\begin{cases}
  \frac{120000}{103.016t + 1250.882} & \mbox{if } t < 9 \\
  -53.542844 \tan (0.046874t - 1.221335) & \mbox{if } t \geq 9
\end{cases}
$$

\subsection{Comparison with data}

\begin{filecontents}{second.txt}
t	vd	vm	d
0	96	95.93228163	0.004585777
1	89	88.63295022	0.13472554
2	82	82.36586538	0.133857474
3	77	76.92652211	0.005399
4	72	72.16109001	0.025949991
5	68	67.95163293	0.002339373
6	64	64.20621676	0.042525352
7	61	60.85211773	0.021869165
8	58	57.83105387	0.028542796
9	55	55.09576962	0.009171821
10	50	50.14048245	0.019735318
11	46	45.62469287	0.140855443
12	41	41.45603245	0.207965591
13	38	37.57956482	0.176765737
14	34	33.95021199	0.002478846
15	31	30.53052199	0.2204096
16	27	27.28900263	0.08352252
17	24	24.19885832	0.039544633
18	21	21.2370184	0.05617772
19	18	18.38337863	0.146979174
20	16	15.62020051	0.144247652
21	13	12.93162799	0.004674732
22	10	10.30329217	0.091986139
23	8	7.721981809	0.077294115
24	5	5.175362674	0.030752068
25	3	2.651732479	0.121290266
26	0	0.139800683	0.019544231
\end{filecontents}

\plotandtable{second.txt}

\vspace{5em}

With this model, it appears that the differences are very small
throughout, leading to the conclusion that the model's assumptions
must be correct, or that any errors in the model's assumptions are too
small to affect the predictions appreciably.

In this case, the sum of the squares of the residuals is $1.99$, which
is very small and adds to the evidence that the second model is
correct in its assumptions.

\subsection{Other strategies for selecting parameter values}
\subsubsection{Substituting known values for $v$ and $t$}

As with the previous model, the most obvious strategy for choosing
values for the parameters is to substitute known values and solve the
resulting equations.  Doing this for the first part of the model
resulted in:

\begin{align*}
v &= \frac{120000}{kt+c} \\
\text{(when $t=0$)}\;\; 96 &= \frac{120000}{c} \\
\text{(when $t=9$)}\;\; 55 &= \frac{120000}{9k+c}
\end{align*}

These equations can be solved, giving $c=1250$ and
$k=103.\overline{53}$. The same can be done for the second part of the
model:

\begin{align*}
v &= a \tan (bt + c) \\
\text{(when $t=9$)} \;\; 55 &= a \tan (9b + c)\\
\text{(when $t=10$)} \;\; 50 &= a \tan (10b + c) \\
\text{(when $t=26$)} \;\; 0 &= a \tan (26b + c)
\end{align*}

These equations can be solved, giving $a=-52.3761$, $b=0.04764$ and
$c=-1.2386$.

This means the new equations are:

$$
v = 
\begin{cases}
  \frac{120000}{103.\overline{53}t+1250} & \mbox{if } t < 9 \\
  -52.3761 \tan (0.04764t -1.2386 ) & \mbox{if } t \geq 9
\end{cases}
$$

These new equations were then compared to the data.

\begin{filecontents}{second_ok.txt}
t	vd	vm	d
0	96	96.0000	0.0000
1	89	88.6567	0.1178
2	82	82.3570	0.1275
3	77	76.8932	0.0114
4	72	72.1093	0.0119
5	68	67.8857	0.0131
6	64	64.1296	0.0168
7	61	60.7673	0.0542
8	58	57.7400	0.0676
9	55	55.0000	0.0000
10	50	50.0000	0.0000
11	46	45.4353	0.3189
12	41	41.2331	0.0544
13	38	37.3350	0.4422
14	34	33.6931	0.0942
15	31	30.2679	0.5359
16	27	27.0264	0.0007
17	24	23.9405	0.0035
18	21	20.9863	0.0002
19	18	18.1428	0.0204
20	16	15.3917	0.3700
21	13	12.7166	0.0803
22	10	10.1027	0.0106
23	8	7.5365	0.2148
24	5	5.0052	0.0000
25	3	2.4969	0.2531
26	0	0.0000	0.0000
\end{filecontents}

\plotandtable{second_ok.txt}

\vspace{2em}

$\Sigma d^2$ for these new equations was $2.8$. This is not too far
off from the value obtained before, which was $2.0$. This seems to
suggest that the model is correct, since substituting in values and
using regression seems to yield almost the same result. Of course,
when substituting values, there is an assumption that the values
subbed in have no error, which is an invalid assumption, hence the
substitution method will always give worse results than using
regression.

\subsubsection{Correcting assumptions made about the distribution of errors in $v$}

As before, the assumption made about the distribution of errors when
calculating the least squares regression line may be incorrect.

\begin{align}
v &= \frac{1}{at+b} + \epsilon_0 \\
\frac{1}{v} &= at+b+\epsilon_1 
\end{align}

By taking equation $(2)$ as suitable for a linear regression analysis,
we have assumed that minimising $\Sigma d^2$ for the second equation
will result in $\Sigma d^2$ for the first equation being
minimised. This will only be the case if the errors in $\frac{1}{v}$,
$\epsilon_1$, are normally distributed (are random, with constant mean
and variance). This may not be the case, as taking the reciprocal of
both sides of equation $(1)$ will definitely have meant that the
distributions of $\epsilon_0$ and $\epsilon_1$ are different. This has
not been taken into account.

As before, a numerical approach must be taken to ensure the best fit,
while making no assumptions about the distribution of errors.  Doing
so, using the same program as before, resulted in the following
equation:

$$v = \frac{120000}{103.2732t+1249.9189}$$

Comparing the results to the data:

\begin{filecontents}{second_better.txt}
t	vd	vm	d
0	96	96.0062	0.0000
1	89	88.6792	0.1029
2	82	82.3912	0.1531
3	77	76.9360	0.0041
4	72	72.1582	0.0250
5	68	67.9392	0.0037
6	64	64.1863	0.0347
7	61	60.8263	0.0302
8	58	57.8006	0.0398
9	55	55.0616	0.0038
10	50	50.1405	0.0197
11	46	45.6247	0.1409
12	41	41.4560	0.2080
13	38	37.5796	0.1768
14	34	33.9502	0.0025
15	31	30.5305	0.2204
16	27	27.2890	0.0835
17	24	24.1989	0.0395
18	21	21.2370	0.0562
19	18	18.3834	0.1470
20	16	15.6202	0.1442
21	13	12.9316	0.0047
22	10	10.3033	0.0920
23	8	7.7220	0.0773
24	5	5.1754	0.0308
25	3	2.6517	0.1213
26	0	0.1398	0.0195
\end{filecontents}

\plotandtable{second_better.txt}
\vspace{2em}

With these parameters, $\Sigma d^2 = 1.98$. For comparison, the first
parameters used gave $\Sigma d^2 = 1.99$. This improvement is
marginal, but does suggest that the manipulation of the equation for
$v$ to give a linear relationship between $\frac{1}{v}$ and $t$ was 
not the best approach, although this could be solely due to rounding
in the given data. It is impossible to gain any information on
whether the errors are indeed normally distributed due to the very
inaccurate data, given to the nearest whole number.

Regardless of whether the first parameters were valid, if we take the
data as it is given, the second parameters give a slightly better fit,
meaning they will be used in the final equations:

$$
v=
\begin{cases}
  \frac{120000}{103.2732t+1249.9189} & \mbox{if } t < 9 \\
  -53.542844 \tan (0.046874t - 1.221335) & \mbox{if } t \geq 9
\end{cases}
$$

\subsection{Comparison with previous model}
$\Sigma d^2$ for the first and second models (with optimal parameters)
are, respectively, $14.8$ and $1.98$.  It is obvious that the second
model is a great improvement over the first model, as the errors in
the predictions are much lower, and they are consistently lower over
the entire range of data points.

With the second model, there is no evidence that the model has been
forced to fit certain data points, instead, the second model seems to fit all
points reasonably well, with a maximum absolute residual of $0.4695$.
This means it has not obviously been forced through any points, as
opposed to the first model, which has a maximum absolute residual of
$1.7689$, even with optimal parameters.

This seems to indicate that the first model is an objectively worse
model, with a far worse fit to the data. This leads me to the conclusion
that the second model is a more acceptable one, with appropriate assumptions
made and parameters found.

\section{Length of the runway}

The minimum length of the runway, based on the optimal parameters for
the second model, is the result of evaluating the following integral:

\begin{align*}
l &= \int_0^9 \frac{120000}{103.2732t+1249.9189} \;dt + \int_9^{26} -53.5428 \tan (0.04687t - 1.2213) \;dt \\
&= \tfrac{120000}{103.2732} \ln (103.2732t+1249.9189) \Big|_0^9 + \tfrac{53.5428}{0.04687} \ln (\cos (0.04687t - 1.2213)) \Big|_9^{26} \\
&= 646 + 412 \\
&= 1058 \, \text{m}
\end{align*}

This is the absolute bare minimum for the length of the runway,
assuming that the runway is only used for landing (as take-off would
require a far greater length), and that the brakes work on every plane
that will land.

To find out the length required if the brakes do not work, we must
work out the value of $t$ when the plane reaches a minimum safe
velocity while obeying the first equation of motion. Assuming 5
ms$^{-1}$ is a safe velocity (safe for taxiing, maneuvering on the ground
etc) that, when reached, marks the end of a successful landing:

\begin{align*}
1 &= \frac{120000}{103.2732t+1249.9189} \\
t &= 220.2903 \, \text{s}
\end{align*}

This means the length of the runway needed when the brakes do not work
is:

\begin{align*}
l &= \int_0^{220.2903} \frac{120000}{103.2732t+1249.9189} \;dt \\
&= \tfrac{120000}{103.2732} \ln (103.2732t+1249.9189) \Big|_0^{220.2903} \\
&= 3436 \, \text{m}
\end{align*}

Considering planes vary greatly in size and shape, it seems reasonable
to round this length up to $4000$ m, for safety.

\end{document}
